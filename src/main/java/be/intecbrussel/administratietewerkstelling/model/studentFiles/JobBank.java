package be.intecbrussel.administratietewerkstelling.model.studentFiles;

import be.intecbrussel.administratietewerkstelling.model.Student;

import javax.persistence.*;

@Entity
public class JobBank {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @OneToOne
    private Student student;
    private boolean submittedCV;
    private boolean firstVersionCV;
    private boolean secondVersionCV;
    private boolean submitMotivation;
    private boolean firstVersionMotivation;
    private boolean secondVersionMotivation;
    private boolean firstExerciseConversationFeedback;
    private boolean firstLinkedinFeedback;
    private boolean secondLinkedinFeedback;

    public JobBank() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public boolean isSubmittedCV() {
        return submittedCV;
    }

    public void setSubmittedCV(boolean submittedCV) {
        this.submittedCV = submittedCV;
    }

    public boolean isFirstVersionCV() {
        return firstVersionCV;
    }

    public void setFirstVersionCV(boolean firstVersionCV) {
        this.firstVersionCV = firstVersionCV;
    }

    public boolean isSecondVersionCV() {
        return secondVersionCV;
    }

    public void setSecondVersionCV(boolean secondVersionCV) {
        this.secondVersionCV = secondVersionCV;
    }

    public boolean isSubmitMotivation() {
        return submitMotivation;
    }

    public void setSubmitMotivation(boolean submitMotivation) {
        this.submitMotivation = submitMotivation;
    }

    public boolean isFirstVersionMotivation() {
        return firstVersionMotivation;
    }

    public void setFirstVersionMotivation(boolean firstVersionMotivation) {
        this.firstVersionMotivation = firstVersionMotivation;
    }

    public boolean isSecondVersionMotivation() {
        return secondVersionMotivation;
    }

    public void setSecondVersionMotivation(boolean secondVersionMotivation) {
        this.secondVersionMotivation = secondVersionMotivation;
    }

    public boolean isFirstExerciseConversationFeedback() {
        return firstExerciseConversationFeedback;
    }

    public void setFirstExerciseConversationFeedback(boolean firstExerciseConversationFeedback) {
        this.firstExerciseConversationFeedback = firstExerciseConversationFeedback;
    }

    public boolean isFirstLinkedinFeedback() {
        return firstLinkedinFeedback;
    }

    public void setFirstLinkedinFeedback(boolean firstLinkedinFeedback) {
        this.firstLinkedinFeedback = firstLinkedinFeedback;
    }

    public boolean isSecondLinkedinFeedback() {
        return secondLinkedinFeedback;
    }

    public void setSecondLinkedinFeedback(boolean secondLinkedinFeedback) {
        this.secondLinkedinFeedback = secondLinkedinFeedback;
    }
}
