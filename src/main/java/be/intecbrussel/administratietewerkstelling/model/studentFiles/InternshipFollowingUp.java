package be.intecbrussel.administratietewerkstelling.model.studentFiles;

import be.intecbrussel.administratietewerkstelling.model.Student;

import javax.persistence.*;
import java.time.LocalDate;
@Entity
public class InternshipFollowingUp {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @OneToOne
    private Student student;
    private boolean internshipMessage;
    private LocalDate firstDay;
    private LocalDate endDay;
    private boolean week1;
    private boolean week2;
    private boolean week3;
    private boolean week4;
    private boolean week5;
    private boolean week6;
    private boolean internshipEvaluation;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public boolean isInternshipMessage() {
        return internshipMessage;
    }

    public void setInternshipMessage(boolean internshipMessage) {
        this.internshipMessage = internshipMessage;
    }

    public LocalDate getFirstDay() {
        return firstDay;
    }

    public void setFirstDay(LocalDate firstDay) {
        this.firstDay = firstDay;
    }

    public LocalDate getEndDay() {
        return endDay;
    }

    public void setEndDay(LocalDate endDay) {
        this.endDay = endDay;
    }

    public boolean isWeek1() {
        return week1;
    }

    public void setWeek1(boolean week1) {
        this.week1 = week1;
    }

    public boolean isWeek2() {
        return week2;
    }

    public void setWeek2(boolean week2) {
        this.week2 = week2;
    }

    public boolean isWeek3() {
        return week3;
    }

    public void setWeek3(boolean week3) {
        this.week3 = week3;
    }

    public boolean isWeek4() {
        return week4;
    }

    public void setWeek4(boolean week4) {
        this.week4 = week4;
    }

    public boolean isWeek5() {
        return week5;
    }

    public void setWeek5(boolean week5) {
        this.week5 = week5;
    }

    public boolean isWeek6() {
        return week6;
    }

    public void setWeek6(boolean week6) {
        this.week6 = week6;
    }

    public boolean isInternshipEvaluation() {
        return internshipEvaluation;
    }

    public void setInternshipEvaluation(boolean internshipEvaluation) {
        this.internshipEvaluation = internshipEvaluation;
    }
}
