package be.intecbrussel.administratietewerkstelling.model.studentFiles;

import be.intecbrussel.administratietewerkstelling.model.Student;

import javax.persistence.*;
import javax.validation.constraints.Pattern;

@Entity
public class StudentProfile {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @OneToOne
    private Student student;
    private String address;
    private String DriversLicense;
    private String languageSkills;
    private String phoneNumber;
    @Pattern(regexp="[A-Za-z0-9._%-+]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}")
    private String email;
    private String itExperience;
    private String workRegion;
    private String notes;
    private String ContactMoments;
    private String PermissionCVToCompanies;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDriversLicense() {
        return DriversLicense;
    }

    public void setDriversLicense(String driversLicense) {
        DriversLicense = driversLicense;
    }

    public String getLanguageSkills() {
        return languageSkills;
    }

    public void setLanguageSkills(String languageSkills) {
        this.languageSkills = languageSkills;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getItExperience() {
        return itExperience;
    }

    public void setItExperience(String itExperience) {
        this.itExperience = itExperience;
    }

    public String getWorkRegion() {
        return workRegion;
    }

    public void setWorkRegion(String workRegion) {
        this.workRegion = workRegion;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getContactMoments() {
        return ContactMoments;
    }

    public void setContactMoments(String contactMoments) {
        ContactMoments = contactMoments;
    }

    public String getPermissionCVToCompanies() {
        return PermissionCVToCompanies;
    }

    public void setPermissionCVToCompanies(String permissionCVToCompanies) {
        PermissionCVToCompanies = permissionCVToCompanies;
    }
}
