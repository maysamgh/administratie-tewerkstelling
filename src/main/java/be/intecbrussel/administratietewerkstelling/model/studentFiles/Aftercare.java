package be.intecbrussel.administratietewerkstelling.model.studentFiles;

import be.intecbrussel.administratietewerkstelling.model.Student;

import javax.persistence.*;
import java.time.LocalDate;
@Entity
public class Aftercare {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @OneToOne
    private Student student;
    private boolean job;
    private LocalDate lastDay;
    private LocalDate endDayAftercare;
    private LocalDate twoMonths;
    private String towMonthsNote;
    private LocalDate fiveMonths;
    private String fiveMonthsNote;
    private LocalDate firstFollowUp;
    private String firstFollowUpNote;
    private LocalDate secondFollowUp;
    private String secondFollowUpNote;
    private LocalDate thirdFollowUp;
    private String thirdFollowUpNote;
    private LocalDate fourthFollowUp;
    private String fourthFollowUpNote;
    private LocalDate fifthFollowUp;
    private String fifthFollowUpNote;
    private LocalDate sixthFollowUp;
    private String sixthFollowUpNote;
    private LocalDate contactVDAB;
    private String contactVDABNote;

    public Aftercare() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public boolean isJob() {
        return job;
    }

    public void setJob(boolean job) {
        this.job = job;
    }

    public LocalDate getLastDay() {
        return lastDay;
    }

    public void setLastDay(LocalDate lastDay) {
        this.lastDay = lastDay;
    }

    public LocalDate getEndDayAftercare() {
        return endDayAftercare;
    }

    public void setEndDayAftercare(LocalDate endDayAftercare) {
        this.endDayAftercare = endDayAftercare;
    }

    public LocalDate isTwoMonths() {
        return twoMonths;
    }

    public void setTwoMonths(LocalDate twoMonths) {
        this.twoMonths = twoMonths;
    }

    public LocalDate getFiveMonths() {
        return fiveMonths;
    }

    public void setFiveMonths(LocalDate fiveMonths) {
        this.fiveMonths = fiveMonths;
    }

    public LocalDate getFirstFollowUp() {
        return firstFollowUp;
    }

    public void setFirstFollowUp(LocalDate firstFollowUp) {
        this.firstFollowUp = firstFollowUp;
    }

    public LocalDate getSecondFollowUp() {
        return secondFollowUp;
    }

    public void setSecondFollowUp(LocalDate secondFollowUp) {
        this.secondFollowUp = secondFollowUp;
    }

    public LocalDate getThirdFollowUp() {
        return thirdFollowUp;
    }

    public void setThirdFollowUp(LocalDate thirdFollowUp) {
        this.thirdFollowUp = thirdFollowUp;
    }

    public LocalDate getFourthFollowUp() {
        return fourthFollowUp;
    }

    public void setFourthFollowUp(LocalDate fourthFollowUp) {
        this.fourthFollowUp = fourthFollowUp;
    }

    public LocalDate getFifthFollowUp() {
        return fifthFollowUp;
    }

    public void setFifthFollowUp(LocalDate fifthFollowUp) {
        this.fifthFollowUp = fifthFollowUp;
    }

    public LocalDate getSixthFollowUp() {
        return sixthFollowUp;
    }

    public void setSixthFollowUp(LocalDate sixthFollowUp) {
        this.sixthFollowUp = sixthFollowUp;
    }

    public LocalDate getContactVDAB() {
        return contactVDAB;
    }

    public void setContactVDAB(LocalDate contactVDAB) {
        this.contactVDAB = contactVDAB;
    }

    public LocalDate getTwoMonths() {
        return twoMonths;
    }

    public String getTowMonthsNote() {
        return towMonthsNote;
    }

    public void setTowMonthsNote(String towMonthsNote) {
        this.towMonthsNote = towMonthsNote;
    }

    public String getFiveMonthsNote() {
        return fiveMonthsNote;
    }

    public void setFiveMonthsNote(String fiveMonthsNote) {
        this.fiveMonthsNote = fiveMonthsNote;
    }

    public String getFirstFollowUpNote() {
        return firstFollowUpNote;
    }

    public void setFirstFollowUpNote(String firstFollowUpNote) {
        this.firstFollowUpNote = firstFollowUpNote;
    }

    public String getSecondFollowUpNote() {
        return secondFollowUpNote;
    }

    public void setSecondFollowUpNote(String secondFollowUpNote) {
        this.secondFollowUpNote = secondFollowUpNote;
    }

    public String getThirdFollowUpNote() {
        return thirdFollowUpNote;
    }

    public void setThirdFollowUpNote(String thirdFollowUpNote) {
        this.thirdFollowUpNote = thirdFollowUpNote;
    }

    public String getFourthFollowUpNote() {
        return fourthFollowUpNote;
    }

    public void setFourthFollowUpNote(String fourthFollowUpNote) {
        this.fourthFollowUpNote = fourthFollowUpNote;
    }

    public String getFifthFollowUpNote() {
        return fifthFollowUpNote;
    }

    public void setFifthFollowUpNote(String fifthFollowUpNote) {
        this.fifthFollowUpNote = fifthFollowUpNote;
    }

    public String getSixthFollowUpNote() {
        return sixthFollowUpNote;
    }

    public void setSixthFollowUpNote(String sixthFollowUpNote) {
        this.sixthFollowUpNote = sixthFollowUpNote;
    }

    public String getContactVDABNote() {
        return contactVDABNote;
    }

    public void setContactVDABNote(String contactVDABNote) {
        this.contactVDABNote = contactVDABNote;
    }
}
