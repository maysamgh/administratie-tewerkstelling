package be.intecbrussel.administratietewerkstelling.model.studentFiles;

import be.intecbrussel.administratietewerkstelling.model.Student;

import javax.persistence.*;

@Entity
public class StartUp {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @OneToOne
    private Student student;
    private boolean IWW;
    private boolean jobProfile;
    private boolean beginTPR;
    private boolean SBTAndOverview;
    private boolean guidanceProgram;
    private boolean uploadMLP;
    private boolean VDABFile;

    public StartUp() {
    }

    public StartUp(boolean IWW, boolean jobProfile, boolean beginTPR, boolean SBTAndOverview, boolean guidanceProgram, boolean uploadMLP, boolean VDABFile) {
        this.IWW = IWW;
        this.jobProfile = jobProfile;
        this.beginTPR = beginTPR;
        this.SBTAndOverview = SBTAndOverview;
        this.guidanceProgram = guidanceProgram;
        this.uploadMLP = uploadMLP;
        this.VDABFile = VDABFile;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public boolean isIWW() {
        return IWW;
    }

    public void setIWW(boolean IWW) {
        this.IWW = IWW;
    }

    public boolean isJobProfile() {
        return jobProfile;
    }

    public void setJobProfile(boolean jobProfile) {
        this.jobProfile = jobProfile;
    }

    public boolean isBeginTPR() {
        return beginTPR;
    }

    public void setBeginTPR(boolean beginTPR) {
        this.beginTPR = beginTPR;
    }

    public boolean isSBTAndOverview() {
        return SBTAndOverview;
    }

    public void setSBTAndOverview(boolean SBTAndOverview) {
        this.SBTAndOverview = SBTAndOverview;
    }

    public boolean isGuidanceProgram() {
        return guidanceProgram;
    }

    public void setGuidanceProgram(boolean guidanceProgram) {
        this.guidanceProgram = guidanceProgram;
    }

    public boolean isUploadMLP() {
        return uploadMLP;
    }

    public void setUploadMLP(boolean uploadMLP) {
        this.uploadMLP = uploadMLP;
    }

    public boolean isVDABFile() {
        return VDABFile;
    }

    public void setVDABFile(boolean VDABFile) {
        this.VDABFile = VDABFile;
    }
}
