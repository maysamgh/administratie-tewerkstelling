package be.intecbrussel.administratietewerkstelling.controllers;

import be.intecbrussel.administratietewerkstelling.model.Training;
import be.intecbrussel.administratietewerkstelling.services.TrainingServiceInterfaceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
public class TrainingController {
    @Autowired
    TrainingServiceInterfaceImpl trainingService;

    @GetMapping("/trainings")
    public String findAllTrainings(Model model) {
        List<Training> trainings = trainingService.findAllTrainings();
        model.addAttribute("listOfTrainings", trainings);
        return "index";
    }

    @PutMapping()
    public String updateTraining(@RequestBody Training training, Model model) {
        trainingService.updateTraining(training);
        return "redirect:/trainings";

    }

    @PostMapping("/new")
    public String saveTraining(@RequestBody Training training, Model model) {
        trainingService.saveTraining(training);
        return "redirect:/trainings";
    }

    @DeleteMapping("/delete/{id}")
    public String deleteTraining(@PathVariable int id, Model model) {
        trainingService.deleteTrainingById(id);
        return "redirect:/trainings";
    }
}
