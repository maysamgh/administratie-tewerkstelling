package be.intecbrussel.administratietewerkstelling;

import be.intecbrussel.administratietewerkstelling.model.Student;
import be.intecbrussel.administratietewerkstelling.model.Training;
import be.intecbrussel.administratietewerkstelling.repositories.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AdministratieTewerkstellingApplication {



    public static void main(String[] args) {
        SpringApplication.run(AdministratieTewerkstellingApplication.class, args);
        StudentRepository studentRepository;
        Training training=new Training();
        training.setName("java developer");
        Student student= new Student();
        student.setName("maysam");
        training.addStudent(student);

    }

}
