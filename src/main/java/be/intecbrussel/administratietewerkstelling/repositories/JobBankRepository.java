package be.intecbrussel.administratietewerkstelling.repositories;

import be.intecbrussel.administratietewerkstelling.model.studentFiles.JobBank;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface JobBankRepository extends JpaRepository<JobBank,Integer> {
}
