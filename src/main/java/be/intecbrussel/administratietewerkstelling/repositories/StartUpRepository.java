package be.intecbrussel.administratietewerkstelling.repositories;

import be.intecbrussel.administratietewerkstelling.model.studentFiles.StartUp;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StartUpRepository extends JpaRepository<StartUp,Integer> {
}
