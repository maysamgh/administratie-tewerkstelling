package be.intecbrussel.administratietewerkstelling.services;

import be.intecbrussel.administratietewerkstelling.interfaces.AftercareServiceInterface;
import be.intecbrussel.administratietewerkstelling.model.studentFiles.Aftercare;
import be.intecbrussel.administratietewerkstelling.repositories.AftercareRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class AftercareServiceInterfaceImpl implements AftercareServiceInterface {
    @Autowired
    AftercareRepository aftercareRepository;

    @Override
    public ResponseEntity<Aftercare> updateAftercare(Aftercare aftercare) {


        if (aftercareRepository.findById(aftercare.getId()).isPresent()) {
            aftercareRepository.findById(aftercare.getId()).get();
            aftercare.setContactVDAB(aftercare.getContactVDAB());
            aftercare.setContactVDABNote(aftercare.getContactVDABNote());
            aftercare.setEndDayAftercare(aftercare.getEndDayAftercare());
            aftercare.setLastDay(aftercare.getLastDay());
            aftercare.setFifthFollowUp(aftercare.getFifthFollowUp());
            aftercare.setFifthFollowUpNote(aftercare.getFifthFollowUpNote());
            aftercare.setFourthFollowUp(aftercare.getFourthFollowUp());
            aftercare.setFourthFollowUpNote(aftercare.getFourthFollowUpNote());
            aftercare.setThirdFollowUp(aftercare.getThirdFollowUp());
            aftercare.setThirdFollowUpNote(aftercare.getThirdFollowUpNote());
            aftercare.setSecondFollowUp(aftercare.getSecondFollowUp());
            aftercare.setSecondFollowUpNote(aftercare.getSecondFollowUpNote());
            aftercare.setFirstFollowUp(aftercare.getFirstFollowUp());
            aftercare.setFirstFollowUpNote(aftercare.getFirstFollowUpNote());

            return new ResponseEntity<>(aftercareRepository.save(aftercare), HttpStatus.OK);
        }else return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }


}
