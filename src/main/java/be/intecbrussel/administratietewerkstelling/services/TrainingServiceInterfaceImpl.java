package be.intecbrussel.administratietewerkstelling.services;

import be.intecbrussel.administratietewerkstelling.interfaces.TrainingServiceInterface;
import be.intecbrussel.administratietewerkstelling.model.Student;
import be.intecbrussel.administratietewerkstelling.model.Training;
import be.intecbrussel.administratietewerkstelling.repositories.TrainingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class TrainingServiceInterfaceImpl implements TrainingServiceInterface {
    @Autowired
    private TrainingRepository trainingRepository;

    public Training saveTraining(Training training) {
        return !trainingRepository.existsById(training.getId()) ? trainingRepository.save(training) : null;
    }

    public void deleteTrainingById(int id) {
        trainingRepository.deleteById(id);
    }

    public ResponseEntity<Training> updateTraining(Training training) {
        if (getTrainingById(training.getId()).isPresent()) {
            getTrainingById(training.getId()).get();
            training.setCeased(training.getCeased());

            training.setThe20Applications(training.getThe20Applications());
            training.setLink(training.getLink());
            training.setJob(training.getJob());
            training.setStartDate(training.getStartDate());
            training.setEndDate(training.getEndDate());

            training.setDigitalFileCheck(training.isDigitalFileCheck());
            training.setDigitalFileInOrder(training.isDigitalFileInOrder());

            training.setOverviewVDABCheck(training.isOverviewVDABCheck());
            training.setOverviewVDABInOrder(training.isOverviewVDABInOrder());

            training.setWallPictureCheck(training.isWallPictureCheck());
            training.setWallPictureInOrder(training.isWallPictureInOrder());

            training.setPlanningCheck(training.isPlanningCheck());
            training.setPlanningInOrder(training.isPlanningInOrder());

            training.setOverviewPictureMapCheck(training.isOverviewPictureMapCheck());
            training.setOverviewVDABInOrder(training.isOverviewPictureMapInorder());

            training.setRedThreadCheck(training.isRedThreadCheck());
            training.setRedThreadInOrder(training.isRedThreadInOrder());
            training.setScoreFormCheck(training.isScoreFormCheck());
            training.setScoreFormInOrder(training.isScoreFormInOrder());
            training.setFileAndBoxCheck(training.isFileAndBoxCheck());
            training.setScoreFormInOrder(training.isFileAndBoxInOrder());

            return new ResponseEntity<>(trainingRepository.save(training), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }



    public Optional<Training> getTrainingById(int id) {
        return trainingRepository.findById(id);
    }

    public List<Training> findAllTrainings() {
        return trainingRepository.findAll();
    }
}
