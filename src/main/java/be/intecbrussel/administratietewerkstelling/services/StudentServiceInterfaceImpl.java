package be.intecbrussel.administratietewerkstelling.services;

import be.intecbrussel.administratietewerkstelling.interfaces.StudentServiceInterface;
import be.intecbrussel.administratietewerkstelling.model.Student;
import be.intecbrussel.administratietewerkstelling.model.Training;
import be.intecbrussel.administratietewerkstelling.repositories.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class StudentServiceInterfaceImpl implements StudentServiceInterface {
    @Autowired
    private StudentRepository studentRepository;
    public Student saveStudent(Training training, Student student) {
        if(!studentRepository.existsById(student.getId()) ){
            training.addStudent(student);
            student.setTraining(training);
            studentRepository.save(student);
            return student;
        }else
          return null;
    }
    public List<Student> findAllStudents(){
        return studentRepository.findAll();
    }
    public List<Student> findStudendsByTraining(Training training){
       return studentRepository.findStudentsByTraining(training);
    }
    public Optional<Student> getStudentById(int id){
       return studentRepository.findById(id);

    }
    public ResponseEntity<Student> deleteStudentById(int id) {
        if (getStudentById(id).isPresent()) {
            studentRepository.deleteById(id);
            return new ResponseEntity(HttpStatus.OK);

        }else {
       return new ResponseEntity(HttpStatus.BAD_REQUEST);
    }}
    public ResponseEntity<Student> updateTheNameOfStudent(Student student){
        if (getStudentById(student.getId()).isPresent()) {
            getStudentById(student.getId()).get();
            student.setName(student.getName());
            return new ResponseEntity<>(studentRepository.save(student), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
}
}
