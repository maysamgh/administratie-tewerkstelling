package be.intecbrussel.administratietewerkstelling.interfaces;

import be.intecbrussel.administratietewerkstelling.model.Training;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.Optional;

public interface TrainingServiceInterface {
    Training saveTraining(Training training);
    void deleteTrainingById(int id);
    ResponseEntity<Training> updateTraining(Training training);
    Optional<Training> getTrainingById(int id);
    List<Training> findAllTrainings();
}
