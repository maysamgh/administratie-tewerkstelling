package be.intecbrussel.administratietewerkstelling.interfaces;

import be.intecbrussel.administratietewerkstelling.model.studentFiles.Aftercare;
import org.springframework.http.ResponseEntity;

public interface AftercareServiceInterface {
    ResponseEntity<Aftercare> updateAftercare(Aftercare aftercare);
}
