package be.intecbrussel.administratietewerkstelling.interfaces;

import be.intecbrussel.administratietewerkstelling.model.Student;
import be.intecbrussel.administratietewerkstelling.model.Training;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.Optional;

public interface StudentServiceInterface {
   Student saveStudent(Training training, Student student);
    List<Student> findAllStudents();
    Optional<Student> getStudentById(int id);
    ResponseEntity<Student> updateTheNameOfStudent(Student student);
    ResponseEntity<Student> deleteStudentById(int id);

}
